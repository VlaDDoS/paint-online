# Paint-online on WebSocket

## Project build
```
docker-compose build
```

### Project start
```
docker-compose up -d
```

### Visit link
```
localhost:3000
```

### Project stop
```
docker-compose down
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

