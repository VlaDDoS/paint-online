import { useEffect } from 'react';
import CanvasState from '@/store/CanvasState';
import axios from 'axios';

export const useSetupCanvas = (canvasEl, params) => {
  useEffect(() => {
    CanvasState.setCanvas(canvasEl.current);
    const ctx = canvasEl.current.getContext('2d');

    axios.get(`http://localhost:5000/image?id=${params.id}`).then((res) => {
      const img = new Image();
      img.src = res.data;

      img.onload = () => {
        ctx.clearRect(0, 0, canvasEl.current.width, canvasEl.current.height);

        ctx.drawImage(
          img,
          0,
          0,
          canvasEl.current.width,
          canvasEl.current.height
        );
      };
    });
  }, []);
};
