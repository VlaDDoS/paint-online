import { useEffect } from 'react';
import CanvasState from '@/store/CanvasState';
import ToolState from '@/store/ToolState';
import { Brush } from '@/tools/Brush';


export const useSetupWS = (canvasEl, params, fn) => {
  useEffect(() => {
    if (CanvasState.userName) {
      const socket = new WebSocket(`ws://localhost:5000`);

      CanvasState.setSocket(socket);
      CanvasState.setSessionId(params.id);

      ToolState.setTool(new Brush(canvasEl.current, socket, params.id));

      socket.addEventListener('open', () => {
        socket.send(
          JSON.stringify({
            id: params.id,
            userName: CanvasState.userName,
            method: 'connection',
          })
        );

        socket.onmessage = (e) => {
          const message = JSON.parse(e.data);

          switch (message.method) {
            case 'connection':
              console.log(`Пользователь ${message.userName} присоединился`);
              break;

            case 'draw':
              fn(message);
              break;
          }
        };
      });
    }
  }, [CanvasState.userName]);
};
