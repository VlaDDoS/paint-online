import { Tool } from './Tool';

export class Line extends Tool {
  static name = 'Line';

  constructor(canvas, socket, id) {
    super(canvas, socket, id);
    this.listen();
  }

  listen() {
    this.canvas.onmousedown = this.mouseDownHandler.bind(this);
    this.canvas.onmouseup = this.mouseUpHandler.bind(this);
    this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
  }

  mouseDownHandler(e) {
    this.mouseDown = true;
    this.ctx.beginPath();

    this.currentX = e.pageX - e.target.offsetLeft;
    this.currentY = e.pageY - e.target.offsetTop;

    this.ctx.moveTo(this.currentX, this.currentY);
    this.saved = this.canvas.toDataURL();
  }

  mouseUpHandler() {
    this.mouseDown = false;

    this.socket.send(
      JSON.stringify({
        id: this.id,
        method: 'draw',
        figure: {
          type: 'finish',
        },
      })
    );
  }

  mouseMoveHandler(e) {
    if (this.mouseDown) {
      this.draw(e.pageX - e.target.offsetLeft, e.pageY - e.target.offsetTop);

      this.socket.send(
        JSON.stringify({
          id: this.id,
          method: 'draw',
          figure: {
            type: Line.name,
            x: this.currentX,
            y: this.currentY,
          },
        })
      );
    }
  }

  draw(x, y) {
    const img = new Image();
    img.src = this.saved;

    img.onload = async () => {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      this.ctx.beginPath();
      this.ctx.moveTo(this.currentX, this.currentY);
      this.ctx.lineTo(x, y);
      this.ctx.stroke();
    };
  }

  static staticDraw(ctx, x, y) {
    ctx.beginPath();
    ctx.moveTo(this.currentX, this.currentY);
    ctx.lineTo(x, y);
    ctx.stroke();
  }
}
