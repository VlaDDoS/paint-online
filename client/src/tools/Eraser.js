import { Brush } from './Brush';

export class Eraser extends Brush {
  static name = 'Eraser';

  constructor(canvas, socket, id) {
    super(canvas, socket, id);
  }

  mouseMoveHandler(e) {
    if (this.mouseDown) {
      this.socket.send(
        JSON.stringify({
          id: this.id,
          method: 'draw',
          figure: {
            type: Eraser.name,
            x: e.pageX - e.target.offsetLeft,
            y: e.pageY - e.target.offsetTop,
            strokeColor: 'white',
            lineWidth: this.ctx.lineWidth,
          },
        })
      );
    }
  }

  static draw(ctx, x, y, strokeColor, lineWidth) {
    ctx.strokeStyle = strokeColor;
    ctx.lineWidth = lineWidth;

    ctx.lineTo(x, y);
    ctx.stroke();
  }
}
