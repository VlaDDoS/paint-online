import { Tool } from './Tool';

export class Brush extends Tool {
  static name = 'Brush';

  constructor(canvas, socket, id) {
    super(canvas, socket, id);
    this.listen();
  }

  listen() {
    this.canvas.onmousedown = this.mouseDownHandler.bind(this);
    this.canvas.onmouseup = this.mouseUpHandler.bind(this);
    this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
  }

  mouseDownHandler(e) {
    this.mouseDown = true;
    this.ctx.beginPath();
    this.ctx.moveTo(
      e.pageX - e.target.offsetLeft,
      e.pageY - e.target.offsetTop
    );
  }

  mouseUpHandler() {
    this.mouseDown = false;

    this.socket.send(
      JSON.stringify({
        id: this.id,
        method: 'draw',
        figure: {
          type: 'finish',
        },
      })
    );
  }

  mouseMoveHandler(e) {
    if (this.mouseDown) {
      this.socket.send(
        JSON.stringify({
          id: this.id,
          method: 'draw',
          figure: {
            type: Brush.name,
            x: e.pageX - e.target.offsetLeft,
            y: e.pageY - e.target.offsetTop,
            strokeColor: this.ctx.strokeStyle,
            lineWidth: this.ctx.lineWidth,
          },
        })
      );
    }
  }

  static draw(ctx, x, y, strokeColor, lineWidth) {
    ctx.strokeStyle = strokeColor;
    ctx.lineWidth = lineWidth;

    ctx.lineTo(x, y);
    ctx.stroke();
  }
}
