import { makeAutoObservable } from 'mobx';

class CanvasState {
  canvas = null;
  socket = null;
  sessionId = null;

  undoList = [];
  redoList = [];

  userName = '';

  constructor() {
    makeAutoObservable(this);
  }

  setCanvas(canvas) {
    this.canvas = canvas;
  }

  setSocket(socket) {
    this.socket = socket;
  }

  setSessionId(id) {
    this.sessionId = id;
  }

  pushToUndo(data) {
    this.undoList.push(data);
  }

  pushToRedo(data) {
    this.redoList.push(data);
  }

  setUserName(userName) {
    this.userName = userName;
  }

  undo() {
    const ctx = this.canvas.getContext('2d');

    if (this.undoList.length > 0) {
      const dataUrl = this.undoList.pop();
      const img = new Image();

      this.redoList.push(this.canvas.toDataURL());

      img.src = dataUrl;
      img.onload = () => {
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      };
    } else {
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
  }

  redo() {
    const ctx = this.canvas.getContext('2d');

    if (this.redoList.length > 0) {
      const dataUrl = this.redoList.pop();
      const img = new Image();

      this.undoList.push(this.canvas.toDataURL());

      img.src = dataUrl;
      img.onload = () => {
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      };
    }
  }
}

export default new CanvasState();
