import { observer } from 'mobx-react-lite';
import React, { useRef } from 'react';
import CanvasState from '@/store/CanvasState';
import { Brush } from '@/tools/Brush';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from 'react-bootstrap';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import { Rect } from '../tools/Rect';
import axios from 'axios';
import { useSetupCanvas } from '../hooks/useSetupCanvas';
import { useSetupWS } from '../hooks/useSetupWS';
import { Circle } from '../tools/Circle';
import { Line } from '../tools/Line';
import { Eraser } from '../tools/Eraser';

export const Canvas = observer(() => {
  const canvasRef = useRef();
  const userNameRef = useRef();
  const params = useParams();

  const [modal, setModal] = useState(true);

  useSetupCanvas(canvasRef, params);

  const drawHandler = (msg) => {
    const figure = msg.figure;
    const ctx = canvasRef.current.getContext('2d');

    switch (figure.type) {
      case Brush.name:
        Brush.draw(
          ctx,
          figure.x,
          figure.y,
          figure.strokeColor,
          figure.lineWidth
        );
        break;

      case Rect.name:
        Rect.staticDraw(
          ctx,
          figure.x,
          figure.y,
          figure.width,
          figure.height,
          figure.color,
          figure.strokeColor,
          figure.lineWidth
        );
        break;

      case Circle.name:
        Circle.staticDraw(
          ctx,
          figure.x,
          figure.y,
          figure.radius,
          figure.color,
          figure.strokeColor,
          figure.lineWidth
        );
        break;

      case Eraser.name:
        Eraser.draw(
          ctx,
          figure.x,
          figure.y,
          figure.strokeColor,
          figure.lineWidth
        );
        break;

      case Line.name:
        Line.staticDraw(ctx, figure.x, figure.y);
        break;

      case 'finish':
        ctx.beginPath();
        break;
    }
  };

  useSetupWS(canvasRef, params, drawHandler);

  const handleMouseDown = () => {
    CanvasState.pushToUndo(canvasRef.current.toDataURL());

    axios
      .post(`http://localhost:5000/image?id=${params.id}`, {
        img: canvasRef.current.toDataURL(),
      })
      .then((res) => console.log(res.data));
  };

  const connectHandler = () => {
    if (userNameRef.current.value.length) {
      CanvasState.setUserName(userNameRef.current.value);
      setModal(false);
    }
  };

  return (
    <div className="canvas">
      <Modal show={modal}>
        <ModalHeader closeButton>
          <ModalTitle>Введите любое имя</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <input type="text" ref={userNameRef} />
        </ModalBody>
        <ModalFooter>
          <Button variant="secondary" onClick={connectHandler}>
            Войти
          </Button>
        </ModalFooter>
      </Modal>

      <canvas
        onMouseDown={handleMouseDown}
        ref={canvasRef}
        width={800}
        height={500}
      ></canvas>
    </div>
  );
});
