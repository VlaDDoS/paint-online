import React from 'react';
import ToolState from '../store/ToolState';

export const SettingsBar = () => {
  const handleLineWidth = (e) => {
    if (+e.target.value >= +e.target.max) {
      ToolState.setLineWidth(e.target.max);
    } else {
      ToolState.setLineWidth(e.target.value);
    }
  };

  const handleChangeStrokeColor = (e) => {
    ToolState.setStrokeColor(e.target.value);
  };

  return (
    <div className="settings">
      <label htmlFor="line-width">Толщина линии</label>
      <input
        defaultValue={1}
        min={1}
        max={50}
        type="number"
        id="line-width"
        className="settings__input"
        onChange={handleLineWidth}
      />

      <label htmlFor="stroke-color">Цвет обводки</label>
      <input
        type="color"
        id="stroke-color"
        className="settings__input"
        onChange={handleChangeStrokeColor}
      />
    </div>
  );
};
