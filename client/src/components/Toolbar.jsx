import { observer } from 'mobx-react-lite';
import React from 'react';
import CanvasState from '../store/CanvasState';
import ToolState from '../store/ToolState';
import { Brush } from '../tools/Brush';
import { Circle } from '../tools/Circle';
import { Eraser } from '../tools/Eraser';
import { Line } from '../tools/Line';
import { Rect } from '../tools/Rect';

export const Toolbar = observer(() => {
  const handleBrush = () => {
    ToolState.setTool(
      new Brush(CanvasState.canvas, CanvasState.socket, CanvasState.sessionId)
    );
  };

  const handleRect = () => {
    ToolState.setTool(
      new Rect(CanvasState.canvas, CanvasState.socket, CanvasState.sessionId)
    );
  };

  const handleCircle = () => {
    ToolState.setTool(
      new Circle(CanvasState.canvas, CanvasState.socket, CanvasState.sessionId)
    );
  };

  const handleEraser = () => {
    ToolState.setTool(
      new Eraser(CanvasState.canvas, CanvasState.socket, CanvasState.sessionId)
    );
  };

  const handleLine = () => {
    ToolState.setTool(
      new Line(CanvasState.canvas, CanvasState.socket, CanvasState.sessionId)
    );
  };

  const handleChangeColor = (e) => {
    ToolState.setStrokeColor(e.target.value);
    ToolState.setFillColor(e.target.value);
  };

  const handleUndo = () => {
    CanvasState.undo();
  };

  const handleRedo = () => {
    CanvasState.redo();
  };

  const handleDownload = () => {
    const dataUrl = CanvasState.canvas.toDataURL();
    const a = document.createElement('a');

    a.href = dataUrl;
    a.download = CanvasState.sessionId + '.jpg';

    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  return (
    <div className="toolbar">
      <button className="toolbar__btn brush" onClick={handleBrush}></button>
      <button className="toolbar__btn rect" onClick={handleRect}></button>
      <button className="toolbar__btn circle" onClick={handleCircle}></button>
      <button className="toolbar__btn eraser" onClick={handleEraser}></button>
      <button className="toolbar__btn line" onClick={handleLine}></button>

      <input onChange={handleChangeColor} type="color" />

      <button className="toolbar__btn undo" onClick={handleUndo}></button>
      <button className="toolbar__btn redo" onClick={handleRedo}></button>
      <button className="toolbar__btn save" onClick={handleDownload}></button>
    </div>
  );
});
